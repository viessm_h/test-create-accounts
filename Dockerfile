# syntax=docker/dockerfile:1.3

# XXX using SUSE BCI is prefered, but without access to package repositories
#     conflicts occur between SLES and OSS
#FROM registry.suse.com/bci/bci-init:15.4
FROM opensuse/leap:15.4

LABEL org.opencontainers.image.source="https://gitlab.psi.ch/viessm_h/test-create-accounts" \
      org.opencontainers.image.title="Test Create Accounts Image" \
      org.opencontainers.image.description="" \
      org.opencontainers.image.authors="HPCE@PSI"

# gracefully kill the container
STOPSIGNAL SIGRTMIN+3

# notify systemd that we are in a container
ENV container docker

# set hostname (sanity check)
ENV HOSTNAME ifyoucanreadthissomethingwentwrong

RUN set -ex \
    #&& zypper addrepo https://download.opensuse.org/distribution/leap/15.4/repo/oss/ oss \
    && zypper addrepo https://download.opensuse.org/repositories/home:/hviess:/psi-hpce-merlin/15.4/home:hviess:psi-hpce-merlin.repo \
    && zypper --gpg-auto-import-keys refresh -s \
    && zypper install -l -y \
       tar gawk wget bzip2 git gpg jq yq \
       python3 python3-pip \
       openssh-server openssh-clients \
       iproute2 hostname file \
       bash-completion \
       vim nano \
       sudo systemd-coredump \
    && zypper clean --all \
    && python3 -m pip install --no-cache-dir --no-compile ansible dnspython mitogen jmespath \
    && rm -rf /usr/share/doc /usr/share/man /tmp/* /var/tmp/* 

# Podman related issue (SUSE container default?), see https://lists.podman.io/archives/list/podman@lists.podman.io/thread/H2I5SNMHI3TANGA7RWX4KZKMIKC3YQLW/
RUN mkdir -p /etc/systemd/system/systemd-logind.service.d/ \
    && echo -e "[Service]\nProtectHostname=no" > /etc/systemd/system/systemd-logind.service.d/10-disable-protecthostname.conf

# add ansible user and wheel group
RUN groupadd -r wheel \
    && useradd -b /var/lib -m -s /bin/bash -U -G wheel ansible \
    && install -m 0700 -o ansible -g ansible -d /var/lib/ansible/.gnupg \
    && install -m 0700 -o ansible -g ansible -d /var/lib/ansible/.ssh \
    && install -m 0755 -o ansible -g ansible -d /var/lib/ansible/roles

# install ansible requirements
COPY requirements.yaml /var/lib/ansible/requirements.yaml
ENV ANSIBLE_ROLES_PATH=/var/lib/ansible/roles
RUN ansible-galaxy install -r /var/lib/ansible/requirements.yaml

# SUSE per-default requires root password for all SUDO operations
# We disable this here
RUN sed -i -e '/^ALL/ s/./#&/' \
           -e '/^Defaults targetpw/ s/./#&/' \
           -e '/wheel.*NOPASSWD/ s/^# *//' /etc/sudoers

# No need for graphical.target here
COPY journal-output.service /etc/systemd/system/journal-output.service
RUN systemctl set-default multi-user.target \
    && systemctl enable journal-output.service \
    && systemctl enable sshd.service

CMD ["/usr/lib/systemd/systemd", "--system", "--log-level=info"]
