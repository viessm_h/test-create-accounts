Test Create Accounts
====================

This repo demonstrates how a `ansible-pull` operations can in integrated into the
deployment of K8s pods.

_Why is this useful?_ Not all configurations or setup procedures can be completely
integrated into typical K8s processes, e.g. natively as YAML, in Helm charts, with
some CD like ArgoCD, etc. This limitations exists though only for the case where you
want to deploy a _system_ container rather then the typical application/service
container.

Typically, if there is some runtime setup that needs doing, the K8s community suggests using
`initContainers` for this. A key condition for this though is that any changes are stored in
some volume so that they are visible/accessible in the pod container. When using a system-container,
some setups touch a lot of file and these cannot be reasonably stored in a volume (unless we
just mount `/`, which is not a great choice).

For a system container, using some IoC tool like ansible is more suitable.

We use `ansible-pull` as this is our choosen tool (Ansible ecosystem) for our configurations
but any similar tool will also fit.

Design
------

Key points:
- ansible-pull is installed in the image
- `postStart` hook is used to do the runtime configuration, calling `ansible-pull`
- some service manager, systemd in our case, provides some daemon process
